# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:
#
# This CI uses the freedesktop.org ci-templates.
# Please see the ci-templates documentation for details:
# https://freedesktop.pages.freedesktop.org/ci-templates/

.templates_sha: &templates_sha 9568e38927f9e9c48d4f586f84a071c3a4bdcd39 # see https://docs.gitlab.com/ee/ci/yaml/#includefile

include:
 - project: 'freedesktop/ci-templates'
   ref: *templates_sha
   file: '/templates/fedora.yml'
 - project: 'freedesktop/ci-templates'
   ref: *templates_sha
   file: '/templates/ci-fairy.yml'
 - template: Security/SAST.gitlab-ci.yml

stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - build            # for actually building and testing things in a container
  - test

variables:
  FDO_UPSTREAM_REPO: 'xorg/app/accessx'
  FDO_DISTRIBUTION_VERSION: 39
  # The tag should be updated each time the list of packages is updated.
  # Changing a tag forces the associated image to be rebuilt.
  # Note: the tag has no meaning, we use a date format purely for readability
  FDO_DISTRIBUTION_TAG: '2024-02-28.0'

#
# Verify that commit messages are as expected
#
check-commits:
  extends:
    - .fdo.ci-fairy
  stage: prep
  script:
    - ci-fairy check-commits --junit-xml=results.xml
  except:
    - master@xorg/app/accessx
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml
  allow_failure: true

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#
check-merge-request:
  extends:
    - .fdo.ci-fairy
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true


#
# Build a container with the given tag and the packages pre-installed.
# This only happens if/when the tag changes, otherwise the existing image is
# re-used.
#
container-prep:
  extends:
  - .fdo.container-build@fedora
  stage: prep
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'libX11-devel libXt-devel motif-devel gcc meson ninja-build'
    GIT_STRATEGY: none
  allow_failure: true

#
# The default build, runs on the image built above.
#
build:
  stage: build
  extends:
  - .fdo.distribution-image@fedora
  needs:
    - container-prep
  script:
    - meson setup builddir
    - cd builddir
    - meson compile
    - meson install
    - meson test
